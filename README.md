# Vector Tiles

This is an experimental new [ChamplainRenderer](https://developer.gnome.org/libchamplain/0.12/ChamplainRenderer.html)
that draws maps from a vector tile source. It currenly uses Mapbox vector
tiles, but it can work with any tiles that use the
[Mapbox Vector Tile Specification](https://github.com/mapbox/vector-tile-spec)
(which is the vast majority).

## Reasoning

Why are vector tiles important? Currently, Champlain uses raster tiles. These
are downloaded as 256x256px PNG or JPG images from a service such as Mapbox.
This service takes data from OpenStreetMap and other sources, and it draws a
tile and serves it over the internet.

This is inefficient and inflexible, however, which is why vector tiles were
created. Instead of downloading an image, programs can download vector data in
a format designed exclusively for map features. They can then render those
features in any way they want.

Here are just some of the possibilities:

- Creating different themes, such as light, dark, and printable
- Developing a custom theme that matches the rest of the GNOME Desktop
- Showing place names in the user's native language where available
- Rotating the map (since labels can be rotated to match)

In addition, vector tiles take much less space than raster tiles, just like an
SVG image takes less space than a PNG. This is especially beneficial for mobile
devices such as the Librem 5, which would typically be downloading the map over
a slow cellular connection. It also makes offline map storage more practical.

These features haven't been implemented yet, but a good vector tile renderer
is the first step to getting there.

# Developing the Map Theme

The theme files are located in the `theme` directory. It is composed of C
files, with heavy use of the macros defined in `vector-macros.h`.

A live preview is included in the source code. To use it, simply click the Run
button in GNOME Builder. If you're not using Builder, do the usual steps to run
meson/ninja, then run `<builddir>/src/vector-previewer`. In the file chooser
that appears, navigate to where you downloaded this repository and select the
`theme/vector-regular.c` file. As you edit and save that file, it will
automatically be recompiled and the map window will be reloaded.

# License

Vector Tiles
Copyright (C) 2019 James Westman

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
USA

#ifndef _vector_draw_context_h_
#define _vector_draw_context_h_

#include <glib-object.h>
#include <cairo.h>
#include "vector-misc.h"
#include "vector-tile-feature.h"
#include "vector-label.h"
#include "vector-label-style.h"

typedef struct {
  cairo_t *cairo;
  gint size;
  gdouble scale;
  const gchar *locale;

  gint zoom;

  VectorTileFeature *feature;

  GList *labels;
  gboolean collecting_labels;

  const gchar *label_text;
  VectorLabelStyle *label_style;
  gint label_rank;
  VectorLabelFlags label_flags;
  const gchar *icon_name;
  gint icon_size;
  VectorColor icon_color;

  VectorColor stroke_color;
  gdouble stroke_width;
  gdouble *stroke_pattern;
  gint stroke_pattern_len;
  gboolean stroke_needed;

  VectorColor fill_color;
  gboolean fill_needed;
} VectorDrawContext;


void vector_draw_context_init (VectorDrawContext *ctx,
                              cairo_t *cairo,
                              gint size,
                              gint zoom);
void vector_draw_context_destroy (VectorDrawContext *ctx);

void vector_draw_context_prepare_draw (VectorDrawContext *ctx);
void vector_draw_context_finish_draw (VectorDrawContext *ctx);

void vector_draw_context_start_feature (VectorDrawContext *ctx,
                                        VectorTileFeature *feature);
void vector_draw_context_finish_feature (VectorDrawContext *ctx);

void vector_draw_context_get_feature_pos (VectorDrawContext *ctx, VectorPoint *res);

void vector_draw_context_fill (VectorDrawContext *ctx);
void vector_draw_context_stroke (VectorDrawContext *ctx);

void vector_draw_context_set_fill_color (VectorDrawContext *ctx, VectorColor color);
void vector_draw_context_set_stroke_color (VectorDrawContext *ctx, VectorColor color);
void vector_draw_context_set_stroke_width (VectorDrawContext *ctx, gdouble width);
void vector_draw_context_set_stroke_pattern (VectorDrawContext *ctx, gdouble *pattern, gint len);

#endif /* _vector_draw_context_h_ */

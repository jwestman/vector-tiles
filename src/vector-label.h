#ifndef _vector_label_h_
#define _vector_label_h_

#include <glib-object.h>
#include "vector-label-style.h"
#include "vector-tile-feature.h"

#define VECTOR_LABEL_RANK_LOWEST INT_MAX

typedef enum {
  VECTOR_LABEL_FLAGS_ICON_ABOVE   = 1 << 0,
  VECTOR_LABEL_FLAGS_FOLLOW_PATH  = 1 << 1,
} VectorLabelFlags;


#define VECTOR_TYPE_LABEL (vector_label_get_type())
G_DECLARE_FINAL_TYPE(VectorLabel, vector_label, VECTOR, LABEL, GObject)

VectorLabel* vector_label_new ();

void vector_label_set_properties (VectorLabel *this,
                                  VectorTileFeature *feature,
                                  const gchar *text,
                                  VectorLabelStyle *style,
                                  int rank,
                                  VectorLabelFlags flags);
void vector_label_set_icon (VectorLabel *this,
                            const gchar *name,
                            gint size,
                            VectorColor color);

VectorLabelFlags vector_label_get_flags(VectorLabel *this);
void vector_label_set_flags(VectorLabel *this, VectorLabelFlags flags);

const gchar* vector_label_get_text (VectorLabel *this);
cairo_surface_t *vector_label_get_icon (VectorLabel *this);
VectorLabelStyle* vector_label_get_style (VectorLabel *this);

VectorTileFeature* vector_label_get_feature (VectorLabel *this);

gint vector_label_compare_rank (VectorLabel* this, VectorLabel *that);

VectorBB *vector_label_get_bb (VectorLabel *this, gboolean create);

#endif /* _vector_label_h_ */

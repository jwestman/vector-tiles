#include <glib-object.h>
#include "vector-tile-layer.h"
#include "vector-tile-feature.h"
#include "vector-parsing.h"

struct _VectorTileLayer {
  GObject parent_instance;

  guint version;

  gchar *name;
  GList *features;
  GHashTable *tag_keys;
  GArray *tag_values;

  guint extent;
};

G_DEFINE_TYPE(VectorTileLayer, vector_tile_layer, G_TYPE_OBJECT)


uint*
vector_tile_layer_get_key_code (VectorTileLayer *this, const gchar *key)
{
  return g_hash_table_lookup (this->tag_keys, key);
}

gchar*
vector_tile_layer_get_tag_value (VectorTileLayer *this, gint tag_code)
{
  if (tag_code >= this->tag_values->len) return NULL;
  return g_array_index (this->tag_values, gchar*, tag_code);
}

/*
 * Frees a slice allocated for type gint.
 */
static void
free_gint_ptr (gpointer data)
{
  g_slice_free (gint, data);
}

void
vector_tile_layer_load (VectorTileLayer *this, VectorDataChunk *data)
{
  this->features = NULL;
  this->tag_keys = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, free_gint_ptr);
  this->tag_values = g_array_new (FALSE, FALSE, sizeof (gchar*));

  gboolean is_done = FALSE;
  gboolean *done = &is_done;
  int tag_key_index = 0;

  VectorDataChunk subdata;
  VectorTileFeature *feature;
  gchar *key;
  gchar *str_val;
  while (!*done) {
    VectorProtobufField field = vector_data_chunk_read_protobuf_field(data, done);

    switch (field.num) {
      case 15:
        // version
        this->version = (uint) vector_data_chunk_read_varint(data, done);
        break;
      case 1:
        // name
        this->name = vector_data_chunk_read_string(data, done);
        break;
      case 2:
        // feature
        vector_data_chunk_read_subchunk(data, &subdata, done);
        feature = vector_tile_feature_new ();
        vector_tile_feature_load (feature, this, &subdata);
        this->features = g_list_prepend (this->features, feature);
        break;
      case 3:
        // keys
        key = vector_data_chunk_read_string (data, done);
        gint *val = g_slice_new (gint);
        *val = tag_key_index ++;
        g_hash_table_insert (this->tag_keys, key, val);
        break;
      case 4:
        // values
        str_val = vector_data_chunk_read_tag_value (data, done);
        g_array_append_val (this->tag_values, str_val);
        break;
      case 5:
        // extent
        this->extent = (guint) vector_data_chunk_read_varint (data, done);
        break;
      default:
        vector_data_chunk_skip_field(data, field, done);
        break;
    }
  }
}


/**
 * vector_tile_layer_get_name:
 * Returns: (transfer none):
 */
gchar*
vector_tile_layer_get_name (VectorTileLayer *this)
{
  return this->name;
}

guint
vector_tile_layer_get_extent (VectorTileLayer *this)
{
  return this->extent;
}

/**
 * vector_tile_layer_get_features:
 * Returns: (transfer none) (element-type VectorTileFeature):
 */
GList*
vector_tile_layer_get_features (VectorTileLayer *this)
{
  return this->features;
}


static void
vector_tile_layer_finalize (GObject *obj)
{
  VectorTileLayer *this = VECTOR_TILE_LAYER (obj);

  if (this->name) g_free (this->name);
  if (this->features) g_list_free_full (this->features, g_object_unref);
  if (this->tag_keys) g_hash_table_unref (this->tag_keys);

  if (this->tag_values) {
    for (int i = 0; i < this->tag_values->len; i ++) {
      g_free (g_array_index (this->tag_values, gchar*, i));
    }
    g_array_unref (this->tag_values);
  }

  G_OBJECT_CLASS (vector_tile_layer_parent_class)->finalize (obj);
}

static void
vector_tile_layer_init (VectorTileLayer *this)
{
}

static void
vector_tile_layer_class_init (VectorTileLayerClass *class)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (class);
  
  obj_class->finalize = vector_tile_layer_finalize;
}

VectorTileLayer *
vector_tile_layer_new (void)
{
  return g_object_new (VECTOR_TYPE_TILE_LAYER, NULL);
}


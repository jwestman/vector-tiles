#ifndef _cairotwisted_h_
#define _cairotwisted_h_

#include <pango/pangocairo.h>

void
draw_twisted (cairo_t *cr,
              gdouble x,
              gdouble y,
              PangoFontDescription *desc,
              const char *text);

#endif /* _cairotwisted_h_ */

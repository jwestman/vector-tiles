#ifndef _vector_definition_h_
#define _vector_definition_h_

#include <glib-object.h>
#include <cairo.h>
#include "vector-misc.h"
#include "vector-draw-context.h"

G_BEGIN_DECLS


#define VECTOR_TYPE_DEFINITION (vector_definition_get_type ())
G_DECLARE_FINAL_TYPE(VectorDefinition, vector_definition, VECTOR, DEFINITION, GObject)

typedef VectorDefinition* (*VectorThemeConstructor)(void);
typedef void (*VectorFunc)(VectorDefinition *theme);
typedef void (*VectorLayerFunc)(VectorDrawContext *ctx);

typedef struct {
  gchar *name;
  VectorLayerFunc func;
} VectorLayer;


VectorDefinition* vector_definition_new (void);

void vector_definition_load (VectorDefinition *theme, VectorFunc func);

void vector_definition_add_layer (VectorDefinition *theme, gchar *name, VectorLayerFunc func);
GList* vector_definition_get_layers (VectorDefinition *theme);
VectorColor* vector_definition_get_background (VectorDefinition *theme);
void vector_definition_set_background (VectorDefinition *theme, VectorColor color);
gint vector_definition_get_layer_count (VectorDefinition *theme);


G_END_DECLS

#endif /* _vector_definition_h_ */

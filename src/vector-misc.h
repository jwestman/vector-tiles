#ifndef _vector_types_h_
#define _vector_types_h_

#include <glib-object.h>

gboolean vector_string_is_one_of (const char *needle, ...);

typedef struct {
  gdouble r;
  gdouble g;
  gdouble b;
} VectorColor;

typedef struct {
  gdouble x;
  gdouble y;
} VectorPoint;

typedef struct {
  gdouble x;
  gdouble y;
  gdouble w;
  gdouble h;
} VectorBB;

gboolean vector_bb_overlaps (VectorBB *this, VectorBB *that);

#endif /* _vector_types_h_ */

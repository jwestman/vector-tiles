#include "../vector-macros.h"

#define BLUE_1    98C1F1
#define BLUE_5    1A5FB4

#define YELLOW_1  F9F06B
#define YELLOW_4  F5C211
#define YELLOW_5  E5A50A

#define RED_1     F66151
#define RED_2     ED333B
#define RED_3     E01B24
#define RED_4     C01C28
#define RED_5     A51D2D

#define PURPLE_5  613583

#define LIGHT_1   FFFFFF
#define LIGHT_2   F6F5F4

#define DARK_1    77767B
#define DARK_4    241F31
#define DARK_5    000000

font (base) {
  font_family ("Cantarell");
  font_size (10);
  text_color (DARK_5);
}

extend_font (base, ocean) {
  font_size (20);
  text_color (LIGHT_2);
  italic ();
  bold ();

  outline_color (BLUE_5);
  outline_width (2);
}
extend_font (ocean, sea) {
  font_size (15);
}

extend_font (base, country) {
  font_size (13);
  text_color (DARK_5);
  bold ();

  outline_color (LIGHT_2);
  outline_width (2);
}

extend_font (base, state) {
  font_size (12);
  text_color (DARK_5);
  bold ();

  outline_color (LIGHT_2);
  outline_width (1);
}


layer (water) {
  fill_color (BLUE_5);
}

layer (natural_label) {
  if (tag_is (class, "ocean")) {
    label (name, ocean);
  } else if (tag_is (class, "sea")) {
    label (name, sea);
  }
}

layer (road) {
  //label (name, streets);
  //label_follow_path ();

  if (tag_is (class, "motorway", "motorway_link", "trunk", "trunk_link")) {
    stroke_color (YELLOW_5);
    stroke_width (3);
  } else if (tag_is (class, "primary", "primary_link")) {
    stroke_color (YELLOW_4);
    stroke_width (2);
  } else {
    stroke_color (YELLOW_1);
    stroke_width (2);
  }

  if (zoom < 6) {
    stroke_width (1);
  }
}

layer (building) {
  fill_color (DARK_1);
  stroke_color (0);
}

layer (place_label) {
  if (tag_is (type, "country")) {
    label (name, country);
  } else if (tag_is (type, "state")) {
    label (name, state);
  }else {
    label (name, base);
  }
}

layer (airport_label) {
  label (name, base);
  icon ("airplane-mode-symbolic", PURPLE_5);
}

layer (admin) {
  if (tag_is (admin_level, "0")) {
    stroke_color (RED_5);

    if (zoom > 4) stroke_width (3);
    else if (zoom > 2) stroke_width (2);

  } else if (tag_is (admin_level, "1")) {
    stroke_color (RED_3);
    if (zoom > 4) stroke_width (2);
  } else {
    stroke_color (RED_1);
    stroke_width (1);
  }

  if (tag_is (disputed, "true")) {
    stroke_pattern (10, 10);
  }

  if (tag_is (maritime, "true")) {
    stroke_color (BLUE_1);
  }
}

theme (regular) {
  background_color (LIGHT_2);

  add_layer (water, water);

  add_layer (road, road);
  add_layer (building, building);

  add_layer (natural_label, natural_label);
  add_layer (place_label, place_label);
  add_layer (airport_label, airport_label);

  add_layer (admin, admin);
}

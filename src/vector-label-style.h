#ifndef _vector_label_style_h_
#define _vector_label_style_h_

#include <glib-object.h>
#include <pango/pango.h>
#include "vector-misc.h"

typedef enum {
  VECTOR_LABEL_DECORATIONS_ITALIC        = 1 << 1,
  VECTOR_LABEL_DECORATIONS_BOLD          = 1 << 2,
  VECTOR_LABEL_DECORATIONS_UNDERLINE     = 1 << 3,
  VECTOR_LABEL_DECORATIONS_STRIKETHROUGH = 1 << 4,
  VECTOR_LABEL_DECORATIONS_SMALL_CAPS    = 1 << 5,
  VECTOR_LABEL_DECORATIONS_OUTLINE       = 1 << 6,
} VectorLabelDecorations;

#define VECTOR_TYPE_LABEL_STYLE (vector_label_style_get_type())
G_DECLARE_FINAL_TYPE(VectorLabelStyle, vector_label_style, VECTOR, LABEL_STYLE, GObject)

VectorLabelStyle* vector_label_style_new ();
VectorLabelStyle* vector_label_style_copy (VectorLabelStyle *this);

void vector_label_style_set_font_family (VectorLabelStyle *label, char *family);
char* vector_label_style_get_font_family (VectorLabelStyle *label);
void vector_label_style_set_font_size (VectorLabelStyle *label, gint size);
gint vector_label_style_get_font_size (VectorLabelStyle *label);
void vector_label_style_set_decorations (VectorLabelStyle *label, VectorLabelDecorations decorations);
void vector_label_style_unset_decorations (VectorLabelStyle *label, VectorLabelDecorations decorations);
VectorLabelDecorations vector_label_style_get_decorations (VectorLabelStyle *label);

void vector_label_style_set_text_color (VectorLabelStyle *label, VectorColor text_color);
VectorColor* vector_label_style_get_text_color (VectorLabelStyle *label);
void vector_label_style_set_outline_color (VectorLabelStyle *label, VectorColor outline_color);
VectorColor* vector_label_style_get_outline_color (VectorLabelStyle *label);
void vector_label_style_set_outline_width (VectorLabelStyle *label, gint outline_width);
gint vector_label_style_get_outline_width (VectorLabelStyle *label);

void vector_label_style_seal (VectorLabelStyle *this);
PangoFontDescription* vector_label_style_get_font_description (VectorLabelStyle *this);

#endif /* _vector_label_style_h_ */

#include "vector-label-style.h"

struct _VectorLabelStyle {
  GObject parent_instance;
  
  gboolean sealed;

  char *font_family;
  gint font_size;

  VectorLabelDecorations decorations;

  VectorColor text_color;
  VectorColor outline_color;
  gint outline_width;

  PangoFontDescription *font_desc;
};

G_DEFINE_TYPE(VectorLabelStyle, vector_label_style, G_TYPE_OBJECT)

#define vector_label_style_check_sealed()  \
  if (this->sealed) {  \
    g_warning ("Trying to modify sealed VectorLabelStyle!");  \
    return;  \
  }


/**
 * vector_label_style_copy:
 *
 * Creates a copy of the #VectorLabelStyle.
 *
 * The copy will not be sealed, so it can be further modified. This is how
 * the extend_font macro works.
 *
 * Returns: (transfer full): A new VectorLabelStyle with the properties of
 * @this
 */
VectorLabelStyle*
vector_label_style_copy (VectorLabelStyle *this)
{
  VectorLabelStyle *res = vector_label_style_new ();

  res->font_family = this->font_family;
  res->font_size = this->font_size;
  res->decorations = this->decorations;
  res->text_color = this->text_color;
  res->outline_color = this->outline_color;
  res->outline_width = this->outline_width;

  return res;
}


void
vector_label_style_set_font_family (VectorLabelStyle *this, char *family)
{
  vector_label_style_check_sealed ();
  this->font_family = family;
}

char*
vector_label_style_get_font_family (VectorLabelStyle *this)
{
  return this->font_family;
}

/**
 * vector_label_style_set_font_size:
 * @size: Font size (pixels)
 * Sets the font size, in pixels, of the label.
 */
void
vector_label_style_set_font_size (VectorLabelStyle *this, gint size)
{
  vector_label_style_check_sealed ();
  this->font_size = size;
}

gint
vector_label_style_get_font_size (VectorLabelStyle *this)
{
  return this->font_size;
}

void
vector_label_style_set_decorations (VectorLabelStyle       *this,
                             VectorLabelDecorations  decorations)
{
  vector_label_style_check_sealed ();
  this->decorations |= decorations;
}

void
vector_label_style_unset_decorations (VectorLabelStyle *this,
                               VectorLabelDecorations decorations)
{
  vector_label_style_check_sealed ();
  this->decorations &= ~decorations;
}

VectorLabelDecorations
vector_label_style_get_decorations (VectorLabelStyle *this)
{
  return this->decorations;
}


void
vector_label_style_set_text_color (VectorLabelStyle *this, VectorColor text_color)
{
  vector_label_style_check_sealed ();
  this->text_color = text_color;
}

/**
 * vector_label_style_get_text_color:
 * Returns: (transfer none):
 */
VectorColor*
vector_label_style_get_text_color (VectorLabelStyle *this)
{
  return &this->text_color;
};

void
vector_label_style_set_outline_color (VectorLabelStyle *this,
                               VectorColor outline_color)
{
  vector_label_style_check_sealed ();
  this->outline_color = outline_color;
  vector_label_style_set_decorations (this, VECTOR_LABEL_DECORATIONS_OUTLINE);
}

/**
 * vector_label_style_get_outline_color:
 * Returns: (transfer none):
 */
VectorColor*
vector_label_style_get_outline_color (VectorLabelStyle *this)
{
  return &this->outline_color;
}

void
vector_label_style_set_outline_width (VectorLabelStyle *this,
                                     gint outline_width)
{
  vector_label_style_check_sealed ();
  this->outline_width = outline_width;
  vector_label_style_set_decorations (this, VECTOR_LABEL_DECORATIONS_OUTLINE);
}

gint
vector_label_style_get_outline_width (VectorLabelStyle *this)
{
  return this->outline_width;
}


/**
 * vector_label_style_seal:
 * Seals the label style, preventing further modification and allowing
 * vector_label_style_get_font_desc() to be called.
 */
void
vector_label_style_seal (VectorLabelStyle *this)
{
  // make sure the label style isn't already sealed
  vector_label_style_check_sealed ();
  this->sealed = TRUE;

  PangoFontDescription *desc = pango_font_description_new ();

  // we can use static since we keep a copy of the string
  pango_font_description_set_family_static (desc, this->font_family);

  // use absolute size for pixels
  pango_font_description_set_absolute_size (desc, this->font_size * PANGO_SCALE);

  if (this->decorations & VECTOR_LABEL_DECORATIONS_SMALL_CAPS) {
    pango_font_description_set_variant (desc, PANGO_VARIANT_SMALL_CAPS);
  }

  if (this->decorations & VECTOR_LABEL_DECORATIONS_BOLD) {
    pango_font_description_set_weight (desc, PANGO_WEIGHT_BOLD);
  }

  if (this->decorations & VECTOR_LABEL_DECORATIONS_ITALIC) {
    pango_font_description_set_style (desc, PANGO_STYLE_ITALIC);
  }

  this->font_desc = desc;
}

/**
 * vector_label_style_get_font_description:
 * Gets the PangoFontDescription describing this label style's font.
 * 
 * Returns: (transfer none):
 */
PangoFontDescription*
vector_label_style_get_font_description (VectorLabelStyle *this)
{
  if (!this->sealed) {
    g_warning ("Cannot get the font description of an unsealed VectorLabelStyle, call vector_label_style_seal() first");
    return NULL;
  }

  return this->font_desc;
}



static void
vector_label_style_init (VectorLabelStyle *this)
{
  this->sealed = FALSE;
}

static void
vector_label_style_finalize (GObject *obj)
{
  VectorLabelStyle *this = VECTOR_LABEL_STYLE (obj);

  if (this->font_desc) pango_font_description_free (this->font_desc);

  G_OBJECT_CLASS (vector_label_style_parent_class)->finalize (obj);
}

static void
vector_label_style_class_init (VectorLabelStyleClass *class)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (class);
  obj_class->finalize = vector_label_style_finalize;
}

VectorLabelStyle *
vector_label_style_new (void)
{
  return g_object_new (VECTOR_TYPE_LABEL_STYLE, NULL);
}


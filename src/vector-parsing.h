#ifndef _vector_parsing_h_
#define _vector_parsing_h_

#include <glib-object.h>
#include "vector-misc.h"

typedef struct {
  guint8 *data;
  guint len;
  guint pos;
} VectorDataChunk;

typedef struct {
  guint num;
  guint wire_type;
} VectorProtobufField;

//gpointer vector_protobuf_field_copy (gpointer data);
//void vector_protobuf_field_free (gpointer data);


void
vector_data_chunk_init (VectorDataChunk *this, guint8 *data, guint len);
void
vector_data_chunk_get_subchunk (VectorDataChunk *this,
                               VectorDataChunk *that,
                               guint64 len,
                               gboolean *done);
void
vector_data_chunk_read_subchunk (VectorDataChunk *this,
                                VectorDataChunk *that,
                                gboolean *done);
GArray* vector_data_chunk_read_uint32_array (VectorDataChunk *this, gboolean *done);
guint8 vector_data_chunk_read_byte (VectorDataChunk *this, gboolean *done);
guint32 vector_data_chunk_read_uint32 (VectorDataChunk *this, gboolean *done);
guint64 vector_data_chunk_read_uint64 (VectorDataChunk *this, gboolean *done);
guint64 vector_data_chunk_read_varint (VectorDataChunk *this, gboolean *done);
gchar* vector_data_chunk_read_string (VectorDataChunk *this, gboolean *done);
VectorProtobufField vector_data_chunk_read_protobuf_field (VectorDataChunk *this, gboolean *done);
void vector_data_chunk_skip_field (VectorDataChunk *this,
                                  VectorProtobufField field,
                                  gboolean *done);
char* vector_data_chunk_read_tag_value (VectorDataChunk *this, gboolean *done);


float vector_uint32_to_float (guint32 u);
double vector_uint64_to_double (guint64 u);
gint64 vector_uint64_to_int64 (guint64 u);
gint32 vector_uint32_to_int32 (guint32 u);
gint32 vector_zigzag_decode_32 (guint32 u);
gint64 vector_zigzag_decode_64 (guint64 u);

#define greater_of(a, b) ((a > b) ? a : b)
#define lesser_of(a, b) ((a < b) ? a : b)

#endif /* _vector_parsing_h_ */

#include <glib-object.h>
#include "vector.h"


void
vector_draw_context_init (VectorDrawContext *this,
                         cairo_t *cairo,
                         gint size,
                         gint zoom)
{
  this->cairo = cairo;
  this->size = size;
  this->zoom = zoom;

  this->labels = NULL;
  this->locale = NULL;
}


/**
 * vector_draw_context_prepare_draw:
 * Should be called each time before drawing the tile.
 */
void
vector_draw_context_prepare_draw (VectorDrawContext *this)
{
  if (!this->labels) this->collecting_labels = TRUE;
}

/**
 * vector_draw_context_finish_draw:
 * Should be called each time after drawing the tile (but before drawing
 * labels).
 */
void
vector_draw_context_finish_draw (VectorDrawContext *this)
{
  if (this->collecting_labels) {
    this->collecting_labels = FALSE;

    this->labels = g_list_sort (this->labels, (GCompareFunc) vector_label_compare_rank);
  }

  cairo_new_path (this->cairo);
}

/**
 * vector_draw_context_next_feature:
 * Clears the VectorDrawContext's properties in preparation for drawing the next
 * feature.
 */
void
vector_draw_context_start_feature (VectorDrawContext *this,
                                   VectorTileFeature *feature)
{
  this->feature = feature;

  this->stroke_width = 1;
  this->stroke_color = (VectorColor) { 0, 0, 0 };
  this->fill_color = (VectorColor) { 0, 0, 0 };
  vector_draw_context_set_stroke_pattern (this, NULL, 0);

  this->fill_needed = FALSE;
  this->stroke_needed = FALSE;

  this->label_text = NULL;
  this->label_style = NULL;
  this->label_rank = VECTOR_LABEL_RANK_LOWEST;
  this->label_flags = 0;
  this->icon_name = NULL;
  this->icon_size = 20;
  this->icon_color = (VectorColor) { 0, 0, 0 };

  cairo_new_path (this->cairo);
  VectorGeometry *geometry = vector_tile_feature_get_geometry (this->feature);
  vector_geometry_draw (geometry, this->cairo);
}

void
vector_draw_context_finish_feature (VectorDrawContext *this)
{
  // Fill and stroke. These will be ignored if they aren't needed, but putting
  // them here means themes don't have to call them explicitly
  vector_draw_context_fill (this);
  vector_draw_context_stroke (this);

  // Add the label, if there was one
  if (this->collecting_labels && (this->label_text || this->icon_name)) {
    if (this->label_rank == VECTOR_LABEL_RANK_LOWEST) {
      const gchar* rank = vector_tile_feature_get_tag (this->feature,
                                                      "symbolrank");
      if (rank) {
        this->label_rank = g_ascii_strtoll (rank, NULL, 10);
      }
    }

    VectorLabel *label = vector_label_new ();
    vector_label_set_properties (label,
                                 this->feature,
                                 this->label_text,
                                 this->label_style,
                                 this->label_rank,
                                 this->label_flags);

    if (this->icon_name) {
      vector_label_set_icon (label,
                             this->icon_name,
                             this->icon_size,
                             this->icon_color);
    }

    this->labels = g_list_prepend (this->labels, label);
  }
}


void
vector_draw_context_get_feature_pos (VectorDrawContext *this, VectorPoint *res)
{
  const VectorPoint *pos = vector_geometry_get_center (vector_tile_feature_get_geometry (this->feature));
  gdouble extent = vector_tile_layer_get_extent (vector_tile_feature_get_parent (this->feature));
  res->x = pos->x / extent;
  res->y = pos->y / extent;
}


void
vector_draw_context_fill (VectorDrawContext *this)
{
  if (!this->fill_needed) return;
  this->fill_needed = FALSE;

  VectorColor rgb = this->fill_color;
  cairo_set_source_rgb (this->cairo, rgb.r, rgb.g, rgb.b);

  cairo_fill_preserve (this->cairo);
}

void
vector_draw_context_stroke (VectorDrawContext *this)
{
  if (!this->stroke_needed) return;
  this->stroke_needed = FALSE;

  VectorColor rgb = this->stroke_color;
  cairo_set_source_rgb(this->cairo, rgb.r, rgb.g, rgb.b);

  cairo_set_line_width (this->cairo, this->stroke_width * this->scale);

  if (this->stroke_pattern) {
    gdouble *pattern = g_memdup (this->stroke_pattern, this->stroke_pattern_len * sizeof (gdouble));

    for (uint i = 0; i < this->stroke_pattern_len; i ++) {
      pattern[i] *= this->scale;
    }
    cairo_set_dash (this->cairo, pattern, this->stroke_pattern_len, 0);

    g_free (pattern);
  } else {
    cairo_set_dash(this->cairo, NULL, 0, 0);
  }

  cairo_stroke_preserve (this->cairo);
}


void
vector_draw_context_set_fill_color (VectorDrawContext *this, VectorColor color)
{
  this->fill_color = color;
  this->fill_needed = TRUE;
}

void
vector_draw_context_set_stroke_color (VectorDrawContext *this, VectorColor color)
{
  this->stroke_color = color;
  this->stroke_needed = TRUE;
}

void
vector_draw_context_set_stroke_width (VectorDrawContext *this, gdouble width)
{
  this->stroke_width = width;
  this->stroke_needed = TRUE;
}

void
vector_draw_context_set_stroke_pattern (VectorDrawContext *this,
                                       gdouble *pattern,
                                       gint len)
{
  this->stroke_pattern = pattern;
  this->stroke_pattern_len = len;
  this->stroke_needed = TRUE;
}

void
vector_draw_context_destroy (VectorDrawContext *this)
{
  if (this->labels) g_list_free_full (this->labels, g_object_unref);
}

#include <stdarg.h>
#include <string.h>
#include <glib.h>
#include "vector-misc.h"

gboolean
vector_string_is_one_of (const char *needle, ...)
{
  if (needle == NULL) return FALSE;

  va_list va;
  va_start (va, needle);

  char *item;
  while ( (item = va_arg (va, char*)) ) {
    if (strcmp (needle, item) == 0) return TRUE;
  }
  return FALSE;
}

gboolean
vector_bb_overlaps (VectorBB *this, VectorBB *that)
{
  return ! ( (that->x > this->x + this->w) ||
             (that->x + that->w < this->x) ||
             (that->y > this->y + this->h) ||
             (that->y + that->h < this->y));
}

#include <glib-object.h>

#include "vector-parsing.h"


/**
 * check_length:
 * @len: (inout):
 *
 * Ensures that @length does not exceed the amount of data left in the
 * stream.
 */
static void
check_length (VectorDataChunk *this, guint64 *len, gboolean *done)
{
  if (this->pos + *len >= this->len) {
      *len = this->len - this->pos;
      *done = TRUE;
  }
}

/**
 * vector_data_chunk_init:
 * @this: (out):
 * @data: (array length=len):
 */
void
vector_data_chunk_init (VectorDataChunk *this, guint8 *data, guint len)
{
  this->data = data;
  this->len = len;
  this->pos = 0;
}

/**
 * vector_data_chunk_get_subchunk:
 * @that: (out):
 * @done: (inout):
 * Creates a substream from the next @length bytes.
 */
void
vector_data_chunk_get_subchunk (VectorDataChunk *this,
                               VectorDataChunk *that,
                               guint64 len,
                               gboolean *done)
{
  check_length(this, &len, done);
  vector_data_chunk_init (that, this->data + this->pos, len);
  this->pos += len;
}

/**
 * vector_data_chunk_read_subchunk:
 * @that: (out):
 * @done: (inout):
 */
void
vector_data_chunk_read_subchunk (VectorDataChunk *this,
                                VectorDataChunk *that,
                                gboolean *done)
{
  guint64 len = vector_data_chunk_read_varint(this, done);
  vector_data_chunk_get_subchunk(this, that, len, done);
}

/**
 * vector_data_chunk_read_uint32_array:
 * @done: (inout):
 * 
 * Reads varints into a new uint32 array. The first varint is the number of
 * bytes to read.
 * 
 * Returns: (element-type guint32) (transfer full):
 */
GArray*
vector_data_chunk_read_uint32_array (VectorDataChunk *this, gboolean *done)
{
  guint64 len = vector_data_chunk_read_varint (this, done);
  VectorDataChunk data;
  vector_data_chunk_get_subchunk (this, &data, len, done);

  // use length/3 as a guesstimate of the amount of size we need
  // this reduces the number of allocations needed
  GArray *array = g_array_sized_new (FALSE, FALSE, sizeof(guint32), len / 3);

  gboolean done2 = FALSE;
  while (!done2) {
    guint32 tmp = (guint32) vector_data_chunk_read_varint (&data, &done2);
    g_array_append_val (array, tmp);
  }

  return array;
}

/**
 * vector_data_chunk_read_byte:
 * @done: (inout):
 */
guint8
vector_data_chunk_read_byte (VectorDataChunk *this, gboolean *done)
{
  if (this->pos >= this->len) {
    *done = TRUE;
    return 0;
  }

  guint8 res = this->data[this->pos];

  this->pos ++;
  if (this->pos >= this->len) *done = TRUE;

  return res;
}

/**
 * vector_data_chunk_read_uint32:
 * @done: (inout):
 */
guint32
vector_data_chunk_read_uint32 (VectorDataChunk *this, gboolean *done)
{
  guint32 res = 0;
  res |= (guint32) vector_data_chunk_read_byte(this, done);
  res |= (guint32) vector_data_chunk_read_byte(this, done) << 8;
  res |= (guint32) vector_data_chunk_read_byte(this, done) << 16;
  res |= (guint32) vector_data_chunk_read_byte(this, done) << 24;
  // protobufs always use little endian
  return GUINT32_FROM_LE (res);
}

/**
 * vector_data_chunk_read_uint64:
 * @done: (inout):
 */
guint64
vector_data_chunk_read_uint64 (VectorDataChunk *this, gboolean *done)
{
  guint64 res = 0;
  res |= (guint64) vector_data_chunk_read_byte(this, done);
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 8;
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 16;
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 24;
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 32;
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 40;
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 48;
  res |= (guint64) vector_data_chunk_read_byte(this, done) << 56;
  // protobufs always use little endian
  return GUINT64_FROM_LE (res);
}

/**
 * vector_data_chunk_read_varint:
 * @done: (inout):
 */
guint64
vector_data_chunk_read_varint (VectorDataChunk *this, gboolean *done)
{
  guint64 res = 0;

  guint8 next;
  guint bits = 0;
  while (!*done) {
    next = vector_data_chunk_read_byte(this, done);

    res |= ((guint64) (next & 127)) << bits;
    bits += 7;

    if (next >> 7 == 0) {
      break;
    }
  }

  return res;
}

/**
 * vector_data_chunk_read_string:
 * @done: (inout):
 * 
 * Reads a string from the DataChunk, prefixed by its length in bytes as a
 * varint. The returned string will be newly allocated and null-terminated.
 * 
 * Returns: (transfer full):
 */
gchar*
vector_data_chunk_read_string (VectorDataChunk *this, gboolean *done)
{
  VectorDataChunk data;
  vector_data_chunk_read_subchunk (this, &data, done);

  if (data.len == 0) return g_strdup ("");

  // add 1 for null char at end
  gchar *str = g_malloc (data.len + 1);

  memcpy (str, data.data, data.len);
  str[data.len] = '\0';
  
  return str;
}

/**
 * vector_data_chunk_read_protobuf_field:
 * @done: (inout):
 * Returns:
 */
VectorProtobufField
vector_data_chunk_read_protobuf_field (VectorDataChunk *this, gboolean *done)
{
  guint varint = (guint) vector_data_chunk_read_varint(this, done);
  return (VectorProtobufField) {
    .num = varint >> 3,
    .wire_type = varint & 7
  };
}

/**
 * vector_data_chunk_skip_field:
 * @done: (inout):
 */
void
vector_data_chunk_skip_field (VectorDataChunk *this,
                             VectorProtobufField field,
                             gboolean *done)
{
  VectorDataChunk useless;

  switch (field.wire_type) {
    case 0:
      // varint
      vector_data_chunk_read_varint(this, done);
      break;
    case 1:
      // 64 bit
      vector_data_chunk_read_uint64(this, done);
      break;
    case 2:
      // length delimimted
      vector_data_chunk_read_subchunk(this, &useless, done);
      break;
    case 5:
      // 32-bit
      vector_data_chunk_read_uint32(this, done);
      break;
  }
}

/**
 * vector_data_chunk_read_tag_value:
 * @done: (inout):
 * 
 * Reads a tag value from the vector tile protobuf format.
 * 
 * Returns: (transfer full):
 */
gchar*
vector_data_chunk_read_tag_value (VectorDataChunk *this, gboolean *done)
{
  // length of Value object in protobuf
  vector_data_chunk_read_varint(this, done);

  GString *res = g_string_new ("");

  VectorProtobufField field = vector_data_chunk_read_protobuf_field (this, done);

  switch (field.num) {
    case 1:
      // string
      g_string_free (res, TRUE);
      return vector_data_chunk_read_string(this, done);
    case 2:
      // float
      g_string_printf (res, "%g", vector_uint32_to_float(vector_data_chunk_read_uint32(this, done)));
      break;
    case 3:
      // double
      g_string_printf (res, "%g", vector_uint64_to_double (vector_data_chunk_read_uint64(this, done)));
      break;
    case 4:
      // int
      g_string_printf (res, "%li", vector_uint64_to_int64 (vector_data_chunk_read_varint(this, done)));
      break;
    case 5:
      // uint
      g_string_printf (res, "%lu", vector_data_chunk_read_varint (this, done));
      break;
    case 6:
      // sint
      g_string_printf (res, "%li", vector_zigzag_decode_64 (vector_data_chunk_read_varint (this, done)));
      break;
    case 7:
      // bool
      g_string_assign (res, vector_data_chunk_read_varint (this, done) == 0 ? "true" : "false");
      break;
    default:
      // huh?
      vector_data_chunk_skip_field(this, field, done);
      g_string_free (res, TRUE);
      return g_strdup ("");
  }

  // free the GString and just return the gchar*
  return g_string_free (res, FALSE);
}


/*
 * Use [type punning](https://en.wikipedia.org/wiki/Type_punning#Raw_CIL_code)
 * to get floats and doubles from ints
 */

float
vector_uint32_to_float(guint32 u) {
  union { float f; guint32 u; } a;
  a.u = u;
  return a.f; 
}

double
vector_uint64_to_double(guint64 u) {
  union { double d; guint64 u; } a;
  a.u = u;
  return a.d;
}

gint64
vector_uint64_to_int64(guint64 u) {
  union { gint64 i; guint64 u; } a;
  a.u = u;
  return a.i;
}

gint32
vector_uint32_to_int32(guint32 u) {
  union { gint64 i; guint64 u; } a;
  a.u = u;
  return a.i;
}

/**
 * zigzag_decode_64:
 * Decodes a zigzag-encoded integer.
 */
gint64
vector_zigzag_decode_64(guint64 u)
{
  return vector_uint64_to_int64( (u >> 1) ^ (-(u & 1)) );
}

/**
 * zigzag_decode_32:
 * Decodes a zigzag-encoded integer.
 */
gint32
vector_zigzag_decode_32(guint32 u)
{
  return vector_uint32_to_int32( (u >> 1) ^ (-(u & 1)) );
}

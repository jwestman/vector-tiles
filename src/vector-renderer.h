#ifndef _vector_renderer_h_
#define _vector_renderer_h_

#include <glib-object.h>
#include <champlain/champlain.h>
#include "vector-parsing.h"
#include "vector-definition.h"


#define VECTOR_TYPE_RENDERER (vector_renderer_get_type())
G_DECLARE_FINAL_TYPE(VectorRenderer, vector_renderer, VECTOR, RENDERER, ChamplainRenderer)

VectorRenderer* vector_renderer_new ();

void vector_renderer_set_locale (VectorRenderer *this, const gchar *locale);

void vector_renderer_set_theme (VectorRenderer *this, VectorDefinition *theme);

#endif /* _vector_renderer_h_ */

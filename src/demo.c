#include <gtk/gtk.h>
#include <glib-object.h>
#include <clutter-gtk/clutter-gtk.h>
#include <champlain-gtk/champlain-gtk.h>

#include "vector.h"

#define SIZE_LIMIT (10 * 1024 * 1024)

extern VectorDefinition *vector_theme_regular_new ();

int
main (int argc, char **argv)
{
  if (gtk_clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS) {
    g_critical ("Could not initialize Clutter!");
    return 1;
  }

  GtkWindow *win = GTK_WINDOW (gtk_window_new (GTK_WINDOW_TOPLEVEL));

  GtkChamplainEmbed *map = GTK_CHAMPLAIN_EMBED (gtk_champlain_embed_new ());
  gtk_widget_set_size_request (GTK_WIDGET (map), 640, 480);
  ChamplainView *view = gtk_champlain_embed_get_view (map);

  VectorDefinition *theme = vector_theme_regular_new ();
  VectorRenderer *render = vector_renderer_new ();
  vector_renderer_set_theme (render, theme);
  vector_renderer_set_locale (render, "en");

  ChamplainNetworkTileSource *src = champlain_network_tile_source_new_full (
      "vector",
      "Vector Tiles",
      "copyright OpenStreetMap contributors, Mapbox",
      "https://www.openstreetmap.org/copyright",
      0,
      18,
      1024,
      CHAMPLAIN_MAP_PROJECTION_MERCATOR,
      "https://a.tiles.mapbox.com/v4/mapbox.mapbox-streets-v8/#Z#/#X#/#Y#.vector.pbf?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
      CHAMPLAIN_RENDERER (render)
  );

  ChamplainFileCache *cache = champlain_file_cache_new_full (10 * 1024 * 1024, NULL, CHAMPLAIN_RENDERER (render));
  ChamplainMemoryCache *memcache = champlain_memory_cache_new_full (100, CHAMPLAIN_RENDERER (render));

  ChamplainMapSourceChain *chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (chain, CHAMPLAIN_MAP_SOURCE (src));
  champlain_map_source_chain_push (chain, CHAMPLAIN_MAP_SOURCE (cache));
  champlain_map_source_chain_push (chain, CHAMPLAIN_MAP_SOURCE (memcache));

  champlain_view_set_horizontal_wrap (view, TRUE);
  champlain_view_set_map_source (view, CHAMPLAIN_MAP_SOURCE (chain));
  champlain_view_center_on (view, 40.6, -74);
  champlain_view_set_zoom_level (view, 9);

  gtk_container_add (GTK_CONTAINER (win), GTK_WIDGET (map));
  gtk_widget_show_all (GTK_WIDGET (win));

  g_signal_connect (win, "destroy", gtk_main_quit, NULL);

  gtk_main ();

  return 0;
}

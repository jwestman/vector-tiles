#ifndef _vector_tile_h_
#define _vector_tile_h_

#include <glib-object.h>
#include <cairo.h>
#include "vector-parsing.h"
#include "vector-definition.h"

#define VECTOR_TYPE_TILE (vector_tile_get_type())
G_DECLARE_FINAL_TYPE(VectorTile, vector_tile, VECTOR, TILE, GObject)

VectorTile* vector_tile_new ();

void vector_tile_load (VectorTile *this,
                      VectorDataChunk *data,
                      VectorDefinition *theme,
                      gint zoom);

void vector_tile_set_locale (VectorTile *this, const gchar *locale);

void vector_tile_draw (VectorTile *this, cairo_t *cairo, gint size);

#endif /* _vector_tile_h_ */

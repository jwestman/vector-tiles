#ifndef __VECTOR_MACROS__
#define __VECTOR_MACROS__

#include "vector-draw-context.h"
#include "vector-definition.h"


#define rgb(r, g, b) ((r << 16) | (g << 8) | b)

#define hex(val) (((0x##val >> 16) & 0xFF) / 255.0), (((0x##val >> 8) & 0xFF) / 255.0), ((0x##val & 0xFF) / 255.0)


#define UNIQUE_NAME_3(x) tmp_##x
#define UNIQUE_NAME_2(x) UNIQUE_NAME_3(x)
#define UNIQUE_NAME UNIQUE_NAME_2(__LINE__)


#define font(name)  \
  static void label_style_##name##_init ();  \
  static VectorLabelStyle* label_style_##name () {  \
    static VectorLabelStyle *label = NULL;  \
    if (label == NULL) {  \
      label = vector_label_style_new ();  \
      label_style_##name##_init (label);  \
      vector_label_style_seal (label);  \
    }  \
    return label;  \
  }  \
  static void label_style_##name##_init (VectorLabelStyle *label)

#define extend_font(base, name)  \
  static void label_style_##name##_init ();  \
  static VectorLabelStyle* label_style_##name () {  \
    static VectorLabelStyle *label = NULL;  \
    if (label == NULL) {  \
      label = vector_label_style_copy (label_style_##base ());  \
      label_style_##name##_init (label);  \
      vector_label_style_seal (label);  \
    }  \
    return label;  \
  }  \
  static void label_style_##name##_init (VectorLabelStyle *label)

#define font_family(val) vector_label_style_set_font_family (label, val);

#define font_size(val) vector_label_style_set_font_size (label, val);


#define italic()  \
  vector_label_style_set_decorations (label, VECTOR_LABEL_DECORATIONS_ITALIC)

#define bold()  \
  vector_label_style_set_decorations (label, VECTOR_LABEL_DECORATIONS_BOLD)

#define underline()  \
  vector_label_style_set_decorations (label, VECTOR_LABEL_DECORATIONS_UNDERLINE)

#define smallcaps()  \
  vector_label_style_set_decorations (label, VECTOR_LABEL_DECORATIONS_SMALL_CAPS)

#define strikethrough()  \
  vector_label_style_set_decorations (label, VECTOR_LABEL_DECORATIONS_STRIKETHROUGH)


#define text_color(c)  \
  vector_label_style_set_text_color (label, (VectorColor) { hex(c) })

#define outline_color(c)  \
  vector_label_style_set_outline_color (label, (VectorColor) { hex(c) })

#define outline_width(w)  \
  vector_label_style_set_outline_width (label, w)




#define layer(name) static void layer_ ## name (VectorDrawContext *ctx)

#define id (vector_tile_feature_get_id (ctx->feature))
#define zoom (ctx->zoom)


#define tag(name) (vector_tile_feature_get_tag (ctx->feature, #name))
#define tag_localized(name) (vector_tile_feature_get_tag_localized (ctx->feature, #name, ctx->locale))
#define tag_is(name, ...) (vector_string_is_one_of (tag (name), __VA_ARGS__, NULL))


#define fill_color(c)  \
  vector_draw_context_set_fill_color (ctx, (VectorColor) { hex(c) })
#define draw_fill()  \
  vector_draw_context_fill (ctx)

#define stroke_color(c) \
  vector_draw_context_set_stroke_color (ctx, (VectorColor) { hex(c) })

#define stroke_width(w)  \
  vector_draw_context_set_stroke_width (ctx, w)

#define stroke_pattern(...)  \
  static gdouble UNIQUE_NAME [] = { __VA_ARGS__ };  \
  vector_draw_context_set_stroke_pattern (ctx, UNIQUE_NAME, sizeof(UNIQUE_NAME) / sizeof(UNIQUE_NAME[0]))

#define draw_stroke()  \
  vector_draw_context_stroke (ctx)


#define label_text(text) ctx->label_text = text

#define label_style(style) ctx->label_style = style

#define label_follow_path() ctx->label_flags |= VECTOR_LABEL_FLAGS_FOLLOW_PATH

#define label_rank(rank) ctx->label_rank = rank

#define label(source_tag, style)  \
  if (ctx->collecting_labels) {  \
    label_text (tag_localized (source_tag));  \
    label_style (label_style_##style ());  \
  }

#define icon_name(text) ctx->icon_name = text

#define icon_size(size) ctx->icon_size = size

#define icon_color(c) ctx->icon_color = (VectorColor) { hex(c) }

#define icon_pos_above() ctx->label_flags |= VECTOR_LABEL_FLAGS_ICON_ABOVE;
#define icon_pos_before() ctx->label_flags &= ~VECTOR_LABEL_FLAGS_ICON_ABOVE;

#define icon(name, color) icon_name (name); icon_color (color)


#define theme(name) \
  static void vector_theme_ ## name (VectorDefinition *theme);  \
  VectorDefinition * vector_theme_ ## name ## _new (void) {  \
    VectorDefinition *theme = vector_definition_new ();  \
    vector_definition_load ( theme, vector_theme_ ## name );  \
    return theme;  \
  }  \
  static void vector_theme_ ## name (VectorDefinition *theme)

#define background_color(c) \
  vector_definition_set_background (theme, (VectorColor) { hex(c) })

#define add_layer(layer_name, func_name)  \
  vector_definition_add_layer (theme, #layer_name, layer_ ## func_name)




#endif /* __VECTOR_MACROS__ */

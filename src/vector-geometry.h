#ifndef _vector_geometry_h_
#define _vector_geometry_h_

#include <glib-object.h>
#include <cairo.h>
#include "vector-misc.h"

typedef enum {
  VECTOR_GEOMETRY_TYPE_UNKNOWN = 0,
  VECTOR_GEOMETRY_TYPE_POINT = 1,
  VECTOR_GEOMETRY_TYPE_LINESTRING = 2,
  VECTOR_GEOMETRY_TYPE_POLYGON = 3,
} VectorGeometryType;

typedef enum {
  VECTOR_COMMAND_MOVE_TO = 1,
  VECTOR_COMMAND_LINE_TO = 2,
  VECTOR_COMMAND_CLOSE_PATH = 7,
} VectorCommand;

typedef struct {
  gdouble length;
  gboolean is_split;
} VectorLineInfo;

typedef gboolean (*VectorPathWalker)(VectorCommand cmd,
                                    gint x,
                                    gint y,
                                    gpointer user_data);


#define VECTOR_TYPE_GEOMETRY (vector_geometry_get_type())
G_DECLARE_FINAL_TYPE(VectorGeometry, vector_geometry, VECTOR, GEOMETRY, GObject)

VectorGeometry *vector_geometry_new ();

void vector_geometry_set_info (VectorGeometry *this,
                              VectorGeometryType type,
                              GArray *data);

void vector_geometry_walk_path (VectorGeometry *this,
                               VectorPathWalker cb,
                               gpointer user_data);

const VectorPoint *vector_geometry_get_center (VectorGeometry *this);
const VectorLineInfo *vector_geometry_get_line_info (VectorGeometry *this);

void vector_geometry_draw (VectorGeometry *this, cairo_t *cairo);

#endif /* _vector_geometry_h_ */

#include <glib-object.h>
#include <cairo.h>
#include <math.h>
#include "vector-geometry.h"
#include "vector-misc.h"
#include "vector-parsing.h"

struct _VectorGeometry {
  GObject parent_instance;

  VectorGeometryType type;
  GArray *data;
  VectorPoint *center;

  VectorLineInfo line_info;
};

G_DEFINE_TYPE(VectorGeometry, vector_geometry, G_TYPE_OBJECT)


/**
 * vector_geometry_set_info:
 * @data: (element-type guint32) (transfer full):
 */
void
vector_geometry_set_info (VectorGeometry *this,
                         VectorGeometryType type,
                         GArray *data)
{
  if (this->data != NULL) {
    g_warning ("Cannot set geometry data twice!");
    return;
  }
  
  this->data = data;
  this->type = type;
}

static gboolean
draw_path_walker (VectorCommand cmd, gint x, gint y, gpointer user_data)
{
  cairo_t *cairo = (cairo_t*) user_data;
  switch (cmd) {
      case VECTOR_COMMAND_MOVE_TO:
          cairo_move_to(cairo, x, y);
          break;
      case VECTOR_COMMAND_LINE_TO:
          cairo_line_to(cairo, x, y);
          break;
      case VECTOR_COMMAND_CLOSE_PATH:
          cairo_close_path(cairo);
          break;
  }
  return FALSE;
}

/**
 * vector_geometry_draw:
 * Adds the geometry to the context's path.
 * 
 * Does not actually paint anything.
 */
void
vector_geometry_draw (VectorGeometry *this, cairo_t *cairo)
{
  cairo_move_to(cairo, 0, 0);
  vector_geometry_walk_path (this, draw_path_walker, cairo);

  if (this->type == VECTOR_GEOMETRY_TYPE_POLYGON) {
    // make sure the path is closed, some tile services seem not to include
    // a final CLOSE_PATH command
    cairo_close_path (cairo);
  }
}


typedef struct {
  gint min_x;
  gint min_y;
  gint max_x;
  gint max_y;
} GetCenterPathWalker;

static gboolean
get_center_path_walker (VectorCommand cmd, gint x, gint y, gpointer user_data)
{
  GetCenterPathWalker *data = (GetCenterPathWalker*) user_data;
  data->min_x = lesser_of (x, data->min_x);
  data->min_y = lesser_of (y, data->min_y);
  data->max_x = greater_of (x, data->max_x);
  data->max_y = greater_of (y, data->max_y);
  return FALSE;
  
}

/**
 * vector_geometry_get_center:
 * Returns some measure of the center of the feature. The current
 * implementation gives the center of the bounding box.
 * 
 * Returns: (transfer none):
 */
const VectorPoint*
vector_geometry_get_center (VectorGeometry *this) {
  if (this->center == NULL) {
    GetCenterPathWalker user_data = {
      .min_x = 8192,
      .min_y = 8192,
      .max_x = -4096,
      .max_y = -4096,
    };

    vector_geometry_walk_path (this, get_center_path_walker, &user_data);

    VectorPoint *center = g_slice_new (VectorPoint);
    *center = (VectorPoint) {
        (user_data.min_x + user_data.max_x) / 2,
        (user_data.min_y + user_data.max_y) / 2
    };
    this->center = center;
  }

  return this->center;
}


typedef struct {
  gint last_x;
  gint last_y;
  gdouble length;
  gboolean is_split;
} LineInfoPathWalker;

static gboolean
line_info_path_walker (VectorCommand cmd, gint x, gint y, gpointer user_data)
{
  LineInfoPathWalker *data = user_data;

  if (cmd != VECTOR_COMMAND_MOVE_TO) {
    data->length += sqrt (pow(x - data->last_x, 2) + pow (y - data->last_y, 2));
  } else {
    if (data->length != 0) {
      // move to command in the middle of the geometry
      data->is_split = TRUE;
    }
  }

  data->last_x = x;
  data->last_y = y;

  return FALSE;
}

/**
 * vector_geometry_get_line_info:
 * @is_split: (out) (nullable): whether the path is split into multiple parts
 *
 * Gets information about the linestring. Returns NULL if the geometry is not
 * a linestring.
 *
 * Returns: length in tile units
 */
const VectorLineInfo*
vector_geometry_get_line_info (VectorGeometry *this)
{
  if (this->line_info.length < 0) {
    LineInfoPathWalker data = {
      .length = 0,
      .is_split = FALSE,
    };
    vector_geometry_walk_path (this, line_info_path_walker, &data);

    this->line_info = (VectorLineInfo) {
      .length = data.length,
      .is_split = data.is_split,
    };
  }

  return &this->line_info;
}


/**
 * vector_geometry_walk_path:
 * @this: a #VectorGeometry
 * @cb: (scope call):
 * @user_data: user_data for @cb
 * 
 * Calls @cb for each point in the geometry in order.
 */
void
vector_geometry_walk_path (VectorGeometry *this,
                          VectorPathWalker cb,
                          gpointer user_data)
{
  int x = 0, y = 0;
  guint32 *data = (guint32*) this->data->data;

  for (gint i = 0; i < this->data->len;) {
    guint32 command_int = data[i++];
    guint command = command_int & 0x7;
    guint count = command_int >> 3;

    for (int j = 0; j < count && i < this->data->len; j ++) {
      switch ((VectorCommand) command) {
        case VECTOR_COMMAND_MOVE_TO:
          if (i + 2 > this->data->len) break;
          x += (int) vector_zigzag_decode_32 (data[i++]);
          y += (int) vector_zigzag_decode_32 (data[i++]);
          if (cb(VECTOR_COMMAND_MOVE_TO, x, y, user_data)) return;
          break;
        case VECTOR_COMMAND_LINE_TO:
          if (i + 2 > this->data->len) break;
          x += (int) vector_zigzag_decode_32 (data[i++]);
          y += (int) vector_zigzag_decode_32 (data[i++]);
          if (cb(VECTOR_COMMAND_LINE_TO, x, y, user_data)) return;
          break;
        case VECTOR_COMMAND_CLOSE_PATH:
          // note that this command does not change the
          // current position
          if (cb(VECTOR_COMMAND_CLOSE_PATH, x, y, user_data)) return;
          break;
      }
    }
  }
}


static void
vector_geometry_finalize (GObject *obj)
{
  VectorGeometry *this = VECTOR_GEOMETRY (obj);

  if (this->center) g_slice_free (VectorPoint, this->center);
  if (this->data) g_array_unref (this->data);

  G_OBJECT_CLASS (vector_geometry_parent_class)->finalize (obj);
}

static void
vector_geometry_init (VectorGeometry *this)
{
  this->line_info = (VectorLineInfo) { .length = -1 };
}

static void
vector_geometry_class_init (VectorGeometryClass *class)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (class);
  obj_class->finalize = vector_geometry_finalize;
}

VectorGeometry *
vector_geometry_new (void)
{
  return g_object_new (VECTOR_TYPE_GEOMETRY, NULL);
}


#include <glib-object.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include <math.h>
#include "cairotwisted.h"
#include "vector-definition.h"
#include "vector-draw-context.h"
#include "vector-geometry.h"
#include "vector-tile.h"
#include "vector-tile-feature.h"
#include "vector-tile-layer.h"

struct _VectorTile {
  GObject parent_instance;

  gint zoom;
  VectorDefinition *theme;
  const gchar *locale;
  GHashTable *layers;
};

G_DEFINE_TYPE(VectorTile, vector_tile, G_TYPE_OBJECT)


void
vector_tile_load (VectorTile *this,
                 VectorDataChunk *data,
                 VectorDefinition *theme,
                 gint zoom)
{
  this->zoom = zoom;
  this->theme = theme;
  g_object_ref (theme);
  this->layers = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_object_unref);

  gboolean is_done = FALSE;
  gboolean *done = &is_done;

  while (!*done) {
    VectorProtobufField field = vector_data_chunk_read_protobuf_field(data, done);
    if (field.num == 3) {
      VectorDataChunk layer_data;
      vector_data_chunk_read_subchunk(data, &layer_data, done);

      VectorTileLayer *layer = vector_tile_layer_new ();
      vector_tile_layer_load (layer, &layer_data);

      gchar *layer_name = vector_tile_layer_get_name (layer);
      g_hash_table_insert (this->layers, layer_name, layer);
    } else {
      vector_data_chunk_skip_field(data, field, done);
    }
  }
}

void
vector_tile_set_locale (VectorTile *this, const gchar *locale)
{
  this->locale = locale;
}


static gboolean
check_bb_overlap (VectorDrawContext *ctx, VectorLabel *overlap_with)
{
  for (GList *labels = ctx->labels; labels; labels = labels->next) {
    VectorLabel *label = (VectorLabel*) labels->data;
    if (label == overlap_with) return FALSE;

    VectorBB *a = vector_label_get_bb (label, FALSE);
    VectorBB *b = vector_label_get_bb (overlap_with, FALSE);
    if (!a || !b) continue;
    if (vector_bb_overlaps (a, b)) {
      return TRUE;
    }
  }

  return FALSE;
}

static void
print_regular_label (VectorTile *this, VectorDrawContext *ctx, VectorLabel *label)
{
  PangoLayout *layout = pango_cairo_create_layout (ctx->cairo);
  VectorLabelStyle *style = vector_label_get_style (label);
  VectorGeometry *geometry = vector_tile_feature_get_geometry (ctx->feature);
  const VectorPoint *pos = vector_geometry_get_center (geometry);

  cairo_translate (ctx->cairo, pos->x, pos->y);
  cairo_scale (ctx->cairo, ctx->scale, ctx->scale);

  pango_layout_set_text(layout, vector_label_get_text (label), -1);
  pango_layout_set_font_description (layout, vector_label_style_get_font_description (style));

  pango_cairo_update_layout (ctx->cairo, layout);
  gint w, h;
  pango_layout_get_pixel_size(layout, &w, &h);

  VectorBB *bb = vector_label_get_bb (label, TRUE);

  gboolean center_horizontal = TRUE;
  if (vector_label_get_icon (label)) {
    gint icon_h = cairo_image_surface_get_height (vector_label_get_icon (label));
    if (vector_label_get_flags (label) & VECTOR_LABEL_FLAGS_ICON_ABOVE) {
      cairo_translate (ctx->cairo, 0, icon_h / 2.0);

      bb->x = pos->x - (w * ctx->scale / 2.0);
      bb->w = w * ctx->scale;
      bb->y = pos->y - (icon_h * ctx->scale / 2.0);
      bb->h = (icon_h + h) * ctx->scale;
    } else {
      gint icon_w = cairo_image_surface_get_width (vector_label_get_icon (label));
      center_horizontal = FALSE;
      cairo_translate (ctx->cairo, icon_w / 2, 0);

      gint height = MAX (h, icon_h) * ctx->scale;
      bb->x = pos->x - (icon_w * ctx->scale / 2.0);
      bb->w = (w + icon_w) * ctx->scale;
      bb->y = pos->y - (height / 2.0);
      bb->h = height;
    }
  } else {
      bb->x = pos->x - (w * ctx->scale / 2.0);
      bb->w = w * ctx->scale;
      bb->y = pos->y - (h * ctx->scale / 2.0);
      bb->h = h * ctx->scale;
  }

  if (!check_bb_overlap (ctx, label)) {
    if (center_horizontal) cairo_translate (ctx->cairo, w / -2.0, h / -2.0);
    else cairo_translate (ctx->cairo, 0, h / -2.0);

    pango_cairo_layout_path (ctx->cairo, layout);
  }
}

static void
print_path_label (VectorTile *this, VectorDrawContext *ctx, VectorLabel *label)
{
  const gchar *text = vector_label_get_text (label);
  VectorLabelStyle *style = vector_label_get_style (label);
  PangoFontDescription *desc = vector_label_style_get_font_description (style);
  VectorGeometry *geometry = vector_tile_feature_get_geometry (ctx->feature);

  const VectorLineInfo *line = vector_geometry_get_line_info (geometry);
  // do not render labels on split paths, it causes rendering problems
  if (line->is_split) return;

  // Scale the font size up by the draw context's scale factor
  PangoFontDescription *desc2 = pango_font_description_copy_static (desc);
  gint font_size = pango_font_description_get_size (desc2) * ctx->scale;
  pango_font_description_set_size (desc2, font_size);

  vector_geometry_draw (geometry, ctx->cairo);
  gdouble offset_y = (font_size / (double) PANGO_SCALE) / 2.0;
  draw_twisted (ctx->cairo, line->length, offset_y, desc2, text);

  pango_font_description_free(desc2);
}

static void
print_label (VectorTile *this, VectorDrawContext *ctx, VectorLabel *label)
{
  if (vector_label_get_flags (label) & VECTOR_LABEL_FLAGS_FOLLOW_PATH) {
    print_path_label (this, ctx, label);
  } else {
    print_regular_label (this, ctx, label);
  }
}

static void
draw_icon (VectorTile *this, VectorDrawContext *ctx, VectorLabel *label)
{
  cairo_surface_t *surf = vector_label_get_icon (label);
  if (!surf) return;

  cairo_save (ctx->cairo);

  VectorTileFeature *feature = vector_label_get_feature (label);
  VectorGeometry *geometry = vector_tile_feature_get_geometry (feature);
  const VectorPoint *center = vector_geometry_get_center (geometry);

  cairo_translate (ctx->cairo, center->x, center->y);

  cairo_scale (ctx->cairo, ctx->scale, ctx->scale);

  gint w = cairo_image_surface_get_width (surf);
  gint h = cairo_image_surface_get_width (surf);
  cairo_translate (ctx->cairo, -w / 2, -h / 2);

  cairo_set_source_surface (ctx->cairo, surf, 0, 0);
  cairo_paint (ctx->cairo);

  cairo_restore (ctx->cairo);
}

static void
draw_labels (VectorTile *this, VectorDrawContext *ctx)
{
  GList *labels = ctx->labels;
  for (GList *item = labels; item; item = item->next) {
    VectorLabel *label = item->data;
    cairo_save (ctx->cairo);

    // Scale the cairo context according to the extents of the layer the label
    // is in
    VectorTileFeature *feature = vector_label_get_feature (label);
    VectorTileLayer *layer = vector_tile_feature_get_parent (feature);
    ctx->scale = vector_tile_layer_get_extent (layer) / (gdouble) ctx->size;
    cairo_scale (ctx->cairo, 1 / ctx->scale, 1 / ctx->scale);
    ctx->feature = feature;

    draw_icon (this, ctx, label);
    if (!vector_label_get_text (label)) {
      cairo_restore (ctx->cairo);
      continue;
    }

    VectorLabelStyle *style = vector_label_get_style (label);
    VectorLabelDecorations decor = vector_label_style_get_decorations (style);

    print_label (this, ctx, label);

    // Draw outline, if there is one
    if ((decor & VECTOR_LABEL_DECORATIONS_OUTLINE) != 0) {
      VectorColor *rgb = vector_label_style_get_outline_color (style);
      cairo_set_source_rgb (ctx->cairo, rgb->r, rgb->g, rgb->b);

      gint stroke = vector_label_style_get_outline_width (style);
      cairo_set_line_width (ctx->cairo, stroke * 2);

      cairo_stroke_preserve (ctx->cairo);
    }

    VectorColor *rgb = vector_label_style_get_text_color (style);
    cairo_set_source_rgb (ctx->cairo, rgb->r, rgb->g, rgb->b);

    cairo_fill (ctx->cairo);

    cairo_restore (ctx->cairo);
  }
}

void
vector_tile_draw (VectorTile *this, cairo_t *cairo, gint size)
{
  cairo_save (cairo);

  VectorDrawContext ctx;
  vector_draw_context_init (&ctx, cairo, size, this->zoom);
  ctx.locale = this->locale;

  // Line cap styles, background color, etc.
  vector_draw_context_prepare_draw (&ctx);
  cairo_set_line_cap (cairo, CAIRO_LINE_CAP_ROUND);
  cairo_set_line_join(cairo, CAIRO_LINE_JOIN_ROUND);

  VectorColor *color = vector_definition_get_background (this->theme);
  cairo_set_source_rgb (cairo, color->r, color->g, color->b);
  cairo_paint (cairo);

  // Render each layer in order
  GList *layers = vector_definition_get_layers (this->theme);
  for (GList *item = layers; item; item = item->next) {
    VectorLayer *vector_layer = item->data;

    cairo_save (cairo);

    VectorTileLayer *layer = g_hash_table_lookup (this->layers, vector_layer->name);

    if (!layer) continue;

    ctx.scale = vector_tile_layer_get_extent (layer) / (gdouble) size;
    cairo_scale (cairo, 1 / ctx.scale, 1 / ctx.scale);

    GList *features = vector_tile_layer_get_features (layer);
    for (GList *item2 = features; item2; item2 = item2->next) {
      VectorTileFeature *f = item2->data;

      vector_draw_context_start_feature (&ctx, f);
      vector_layer->func (&ctx);
      vector_draw_context_finish_feature (&ctx);
    }

    cairo_restore (cairo);
  }

  vector_draw_context_finish_draw (&ctx);

  draw_labels (this, &ctx);

  cairo_restore (cairo);

  vector_draw_context_destroy (&ctx);
}



static void
vector_tile_finalize (GObject *obj)
{
  VectorTile *this = VECTOR_TILE (obj);

  if (this->theme) g_object_unref (this->theme);
  if (this->layers) g_hash_table_unref (this->layers);

  G_OBJECT_CLASS (vector_tile_parent_class)->finalize (obj);
}

static void
vector_tile_init (VectorTile *this)
{
  this->locale = NULL;
}

static void
vector_tile_class_init (VectorTileClass *class)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (class);

  obj_class->finalize = vector_tile_finalize;
}

VectorTile *
vector_tile_new (void)
{
  return g_object_new (VECTOR_TYPE_TILE, NULL);
}


#include <gtk/gtk.h>

#include "vector-label.h"
#include "vector-label-style.h"
#include "vector-tile-feature.h"

struct _VectorLabel {
  GObject parent_instance;

  const gchar *text;
  VectorLabelStyle *style;
  int rank;

  cairo_surface_t *icon;

  VectorLabelFlags flags;

  VectorTileFeature *feature;

  VectorBB *bb;
};

G_DEFINE_TYPE(VectorLabel, vector_label, G_TYPE_OBJECT)


void
vector_label_set_properties (VectorLabel *this,
                             VectorTileFeature *feature,
                             const gchar *text,
                             VectorLabelStyle *style,
                             int rank,
                             VectorLabelFlags flags)
{
  this->feature = feature;
  g_object_ref (feature);

  this->text = text;
  this->style = style;
  this->rank = rank;
  this->flags = flags;
}

void vector_label_set_icon (VectorLabel *this,
                            const gchar *name,
                            gint size,
                            VectorColor color)
{
  GtkIconTheme *theme = gtk_icon_theme_get_default ();
  GtkIconInfo *info = gtk_icon_theme_lookup_icon (theme, name, size, 0);

  if (!info) return;

  GdkRGBA rgba;
  rgba.red = color.r;
  rgba.green = color.g;
  rgba.blue = color.b;
  rgba.alpha = 1;

  GError *err = NULL;
  GdkPixbuf *pixbuf = gtk_icon_info_load_symbolic (info, &rgba,
                                                   NULL, NULL, NULL, NULL,
                                                   &err);
  if (err) return;

  this->icon = gdk_cairo_surface_create_from_pixbuf (pixbuf, 0, NULL);

  g_object_unref (pixbuf);
  g_object_unref (info);
}

VectorLabelFlags
vector_label_get_flags (VectorLabel *this)
{
  return this->flags;
}

void
vector_label_set_flags (VectorLabel *this, VectorLabelFlags flags)
{
  this->flags = flags;
}

/**
 * vector_label_get_text:
 * Returns: (transfer none) (nullable):
 */
const gchar*
vector_label_get_text (VectorLabel *this)
{
  return this->text;
}

/**
 * vector_label_get_icon:
 * Returns the label's icon, or NULL if it has no icon.
 *
 * Returns: (transfer none) (nullable):
 */
cairo_surface_t *
vector_label_get_icon (VectorLabel *this)
{
  return this->icon;
}

/**
 * vector_label_get_style:
 * Returns: (transfer none):
 */
VectorLabelStyle*
vector_label_get_style (VectorLabel *this)
{
  return this->style;
}

/**
 * vector_label_get_feature:
 * Returns: (transfer none):
 */
VectorTileFeature*
vector_label_get_feature (VectorLabel *this)
{
  return this->feature;
}

VectorBB *
vector_label_get_bb (VectorLabel *this, gboolean create)
{
  if (this->bb == NULL && create) this->bb = g_slice_new0 (VectorBB);
  return this->bb;
}


gint
vector_label_compare_rank (VectorLabel* this, VectorLabel *that)
{
  return this->rank - that->rank;
}


static void
vector_label_finalize (GObject *obj)
{
  VectorLabel *this = VECTOR_LABEL (obj);

  if (this->feature) g_object_unref (this->feature);
  if (this->icon) cairo_surface_destroy (this->icon);
  if (this->bb) g_slice_free (VectorBB, this->bb);

  G_OBJECT_CLASS (vector_label_parent_class)->finalize (obj);
}

static void
vector_label_init (VectorLabel *this)
{
  this->bb = NULL;
}

static void
vector_label_class_init (VectorLabelClass *class)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (class);
  obj_class->finalize = vector_label_finalize;
}

VectorLabel *
vector_label_new (void)
{
  return g_object_new (VECTOR_TYPE_LABEL, NULL);
}


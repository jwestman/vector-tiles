#ifndef _vector_tile_layer_h_
#define _vector_tile_layer_h_

#include <glib-object.h>
#include "vector-parsing.h"

// circular dependency from theme-tile-feature.h
struct VectorTileFeature;

#define VECTOR_TYPE_TILE_LAYER (vector_tile_layer_get_type())
G_DECLARE_FINAL_TYPE(VectorTileLayer, vector_tile_layer, VECTOR, TILE_LAYER, GObject)

VectorTileLayer* vector_tile_layer_new ();

void vector_tile_layer_load (VectorTileLayer *this, VectorDataChunk *data);

gchar *vector_tile_layer_get_tag_value (VectorTileLayer *this, gint tag_code);
uint *vector_tile_layer_get_key_code (VectorTileLayer *this, const gchar *key);

gchar *vector_tile_layer_get_name (VectorTileLayer *this);
guint vector_tile_layer_get_extent (VectorTileLayer *this);
GList *vector_tile_layer_get_features (VectorTileLayer *this);


#endif /* _vector_tile_layer_h_ */

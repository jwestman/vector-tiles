#include <glib-object.h>
#include "vector-tile-feature.h"
#include "vector-geometry.h"
#include "vector-parsing.h"

// circular dependencies from theme-tile-layer.h
gchar *vector_tile_layer_get_tag_value (VectorTileLayer *this, gint tag_code);
uint *vector_tile_layer_get_key_code (VectorTileLayer *this, const gchar *key);

struct _VectorTileFeature {
  GObject parent_instance;

  guint64 id;

  GHashTable *tags;
  // keys and values in :tags point to items in this array
  // it must be unreffed on finalization
  GArray *tag_data;

  VectorTileLayer *parent;
  VectorGeometry *geometry;
};

G_DEFINE_TYPE(VectorTileFeature, vector_tile_feature, G_TYPE_OBJECT)


/**
 * vector_tile_feature_get_tag:
 *
 * Gets the value of a tag for the feature.
 *
 * Returns: (nullable): The value of the tag, or null if it is not defined
 * for this feature
 */
const gchar*
vector_tile_feature_get_tag (VectorTileFeature *this, const gchar *key)
{
  guint32 *key_code = vector_tile_layer_get_key_code (this->parent, key);
  if (!key_code) return NULL;

  if (!g_hash_table_contains(this->tags, key_code)) return NULL;
  guint32 *val_code = g_hash_table_lookup (this->tags, key_code);

  return vector_tile_layer_get_tag_value (this->parent, *val_code);
}

/**
 * vector_tile_feature_get_tag_localized:
 * Tries to get a localized version of a tag.
 *
 * First, tries forming a localized tag key like `name:en`. If that is not
 * defined for the feature, then falls back on just the provided key.
 *
 * For convenience, @locale may be NULL. This will make the function act like
 * the regular #VectorTileFeature.get_tag().
 */
const gchar*
vector_tile_feature_get_tag_localized (VectorTileFeature *this,
                                       const gchar *key,
                                       const gchar *locale)
{
  if (locale == NULL) return vector_tile_feature_get_tag (this, key);

  GString *key_str = g_string_sized_new (strlen (key) + strlen (locale) + 2);

  g_string_assign (key_str, key);
  g_string_append (key_str, "_");
  g_string_append (key_str, locale);

  const gchar *val = vector_tile_feature_get_tag (this, key_str->str);
  if (val == NULL) {
    val = vector_tile_feature_get_tag (this, key);
  }

  g_string_free (key_str, TRUE);
  return val;
}


void
vector_tile_feature_load (VectorTileFeature *this,
                         VectorTileLayer *parent,
                         VectorDataChunk *data)
{
  this->parent = parent;
  
  this->tags = g_hash_table_new (g_int_hash, g_int_equal);

  gboolean is_done = FALSE;
  gboolean *done = &is_done;

  VectorGeometryType geometry_type = VECTOR_GEOMETRY_TYPE_UNKNOWN;
  GArray *geometry_data = NULL;

  while (!*done) {
      VectorProtobufField field = vector_data_chunk_read_protobuf_field(data, done);

      switch (field.num) {
          case 1:
              // id
              this->id = vector_data_chunk_read_varint(data, done);
              break;
          case 2:
              // tags
              if (this->tag_data) g_array_unref (this->tag_data);

              this->tag_data = vector_data_chunk_read_uint32_array(data, done);
              for (int i = 0; i < this->tag_data->len - 1; i += 2) {
                  g_hash_table_insert(this->tags, &((guint32*)this->tag_data->data)[i], &((guint32*)this->tag_data->data)[i + 1]);
              }
              break;
          case 3:
              // geometry type
              geometry_type = (VectorGeometryType) vector_data_chunk_read_varint(data, done);
              break;
          case 4:
              // geometry data
              if (geometry_data) g_array_unref (geometry_data);
              geometry_data = vector_data_chunk_read_uint32_array(data, done);
              break;
          default:
              vector_data_chunk_skip_field(data, field, done);
              break;
      }
  }

  if (geometry_data != NULL) {
      this->geometry = vector_geometry_new ();
      vector_geometry_set_info(this->geometry, geometry_type, geometry_data);
  }
}


/**
 * vector_tile_feature_get_id:
 */
guint64
vector_tile_feature_get_id (VectorTileFeature *this)
{
  return this->id;
}

/**
 * vector_tile_feature_get_parent:
 * Returns: (transfer none):
 */
VectorTileLayer*
vector_tile_feature_get_parent (VectorTileFeature *this)
{
  return this->parent;
}

/**
 * vector_tile_feature_get_geometry:
 * Returns: (transfer none):
 */
VectorGeometry*
vector_tile_feature_get_geometry (VectorTileFeature *this)
{
  return this->geometry;
}


static void
vector_tile_feature_finalize (GObject *obj)
{
  VectorTileFeature *this = VECTOR_TILE_FEATURE (obj);

  if (this->tags) g_hash_table_unref (this->tags);
  if (this->tag_data) g_array_unref (this->tag_data);
  if (this->geometry) g_object_unref (this->geometry);

  G_OBJECT_CLASS (vector_tile_feature_parent_class)->finalize (obj);
}

static void
vector_tile_feature_init (VectorTileFeature *this)
{
}

static void
vector_tile_feature_class_init (VectorTileFeatureClass *class)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (class);
  obj_class->finalize = vector_tile_feature_finalize;
}

VectorTileFeature *
vector_tile_feature_new (void)
{
  return g_object_new (VECTOR_TYPE_TILE_FEATURE, NULL);
}


/*
 * DISCLAIMER: The code quality here is awful! This program is a quick hack to
 * make it easier to iterate on theme designs. It is NOT an example of how to
 * properly use libchamplain or the vector tile renderer. You have been warned.
 */

#include <gtk/gtk.h>
#include <clutter-gtk/clutter-gtk.h>
#include <champlain-gtk/champlain-gtk.h>

#include "vector.h"

GtkChamplainEmbed *map;
ChamplainView *view;
gchar **compile;
gchar *so_file;
VectorThemeConstructor theme_constructor;
GModule *module = NULL;
ChamplainView *view;

static void
build_compile_command (GFile *file) {
  GError *err = NULL;
  gchar *filename = g_file_get_path (file);

  // Load pkg-config info
  GSubprocess *pkgconfig = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE, &err,
    "pkg-config", "--libs", "--cflags", "gobject-2.0", "cairo", "pango",
  NULL);

  gchar *buf = NULL;
  g_subprocess_communicate_utf8 (pkgconfig, NULL, NULL, &buf, NULL, &err);

  gchar **ccflags = g_strsplit (buf, " ", 0);
  gint ccflags_num = 0;
  while (ccflags[ccflags_num]) ccflags_num ++;


  // Create temporary file for shared library
  GFileIOStream *tmpstream = NULL;
  GFile *tmp = g_file_new_tmp ("XXXXXX.so", &tmpstream, &err);
  so_file = g_file_get_path (tmp);

  GFile *parent = g_file_get_parent (file);
  gchar *parent_path = g_file_get_path (parent);

  gchar *include = g_strconcat ("-I", parent_path, NULL);

  compile = g_new (gchar*, ccflags_num + 8);
  compile[0] = "gcc";
  compile[1] = "-Werror=implicit-function-declaration";
  compile[2] = "-shared";
  compile[3] = "-o";
  compile[4] = so_file;
  compile[5] = "-fPIC";
  compile[6] = filename;
  compile[7] = include;
  for (int i = 0; ccflags[i]; i ++) {
    compile [8 + i] = g_strdup (ccflags[i]);
  }
  compile[ccflags_num + 7] = NULL;

  if (err) g_error_free (err);
  g_free (buf);
  g_object_unref (tmpstream);
  g_object_unref (parent);
  g_object_unref (tmp);
  g_strfreev (ccflags);
  g_free (parent_path);
}

static void
run_compiler () {
  GError *err = NULL;

  GSubprocess *gcc = g_subprocess_newv ((const gchar * const *) compile, G_SUBPROCESS_FLAGS_STDOUT_PIPE, &err);
  g_subprocess_wait (gcc, NULL, &err);
  g_object_unref (gcc);

  if (module != NULL) g_module_close (module);
  module = g_module_open (so_file, G_MODULE_BIND_LAZY);
  if (!g_module_symbol (module, "vector_theme_regular_new", (gpointer *) &theme_constructor)
      || theme_constructor == NULL) {
    g_warning ("Could not load vector_theme_regular_new, please name your theme `regular` or edit vector-previewer.c");
  }

  if (err) {
    g_debug ("There was an error! %s", err->message);
    g_error_free (err);
  }


  // now update the window
  VectorDefinition *theme = theme_constructor ();
  VectorRenderer *render = vector_renderer_new ();
  vector_renderer_set_theme (render, theme);
  vector_renderer_set_locale (render, "en");

  ChamplainNetworkTileSource *src = champlain_network_tile_source_new_full (
      "vector",
      "Vector Tiles",
      "copyright OpenStreetMap contributors, Mapbox",
      "https://www.openstreetmap.org/copyright",
      0,
      18,
      1024,
      CHAMPLAIN_MAP_PROJECTION_MERCATOR,
      "https://a.tiles.mapbox.com/v4/mapbox.mapbox-streets-v8/#Z#/#X#/#Y#.vector.pbf?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
      CHAMPLAIN_RENDERER (render)
  );

  ChamplainFileCache *cache = champlain_file_cache_new_full (10 * 1024 * 1024, NULL, CHAMPLAIN_RENDERER (render));
  ChamplainMemoryCache *memcache = champlain_memory_cache_new_full (100, CHAMPLAIN_RENDERER (render));

  ChamplainMapSourceChain *chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (chain, CHAMPLAIN_MAP_SOURCE (src));
  champlain_map_source_chain_push (chain, CHAMPLAIN_MAP_SOURCE (cache));
  champlain_map_source_chain_push (chain, CHAMPLAIN_MAP_SOURCE (memcache));

  champlain_view_set_map_source (view, CHAMPLAIN_MAP_SOURCE (chain));
}

static void
create_map_window () {
  if (gtk_clutter_init (NULL, NULL) != CLUTTER_INIT_SUCCESS) {
    g_critical ("Could not initialize Clutter!");
    return;
  }

  GtkWindow *win = GTK_WINDOW (gtk_window_new (GTK_WINDOW_TOPLEVEL));

  GtkChamplainEmbed *map = GTK_CHAMPLAIN_EMBED (gtk_champlain_embed_new ());
  gtk_widget_set_size_request (GTK_WIDGET (map), 640, 480);
  view = gtk_champlain_embed_get_view (map);

  champlain_view_set_horizontal_wrap (view, TRUE);
  champlain_view_center_on (view, 40.6, -74);
  champlain_view_set_zoom_level (view, 9);

  GtkWidget *label = gtk_label_new ("Zoom");
  g_object_set (label, "halign", GTK_ALIGN_START, NULL);

  g_object_bind_property (view, "zoom-level", label, "label", G_BINDING_DEFAULT);

  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add (GTK_CONTAINER (box), label);
  gtk_container_add (GTK_CONTAINER (box), GTK_WIDGET (map));

  gtk_container_add (GTK_CONTAINER (win), GTK_WIDGET (box));
  gtk_widget_show_all (GTK_WIDGET (win));

  g_signal_connect (win, "destroy", gtk_main_quit, NULL);
}

void
on_file_changed (GFileMonitor *mon,
                 GFile *file,
                 GFile *other_file,
                 GFileMonitorEvent event_type,
                 gpointer user_data) {
  run_compiler ();
}

int
main (int argc, char **argv) {
  gtk_init (&argc, &argv);

  GError *err = NULL;

  GtkWidget *filechooser = gtk_file_chooser_dialog_new (
    "Find vector-regular.c",
    NULL,
    GTK_FILE_CHOOSER_ACTION_OPEN,
    "Cancel", GTK_RESPONSE_CANCEL,
    "Open", GTK_RESPONSE_ACCEPT,
  NULL);

  int result = gtk_dialog_run (GTK_DIALOG (filechooser));
  if (result == GTK_RESPONSE_ACCEPT) {
    gchar *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (filechooser));
    gtk_widget_destroy (filechooser);

    GFile *file = g_file_new_for_path (filename);

    build_compile_command (file);

    create_map_window ();

    GFileMonitor *mon = g_file_monitor (file, G_FILE_MONITOR_NONE, NULL, &err);
    g_signal_connect (mon, "changed", (GCallback) on_file_changed, NULL);
    run_compiler ();

    gtk_main ();
  } else {
    gtk_widget_destroy (filechooser);
  }

  return 0;
}

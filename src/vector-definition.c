#include "vector.h"


struct _VectorDefinition
{
  GObject parent_instance;

  GList *layers;

  VectorColor background;
};

G_DEFINE_TYPE(VectorDefinition, vector_definition, G_TYPE_OBJECT)

/**
 * vector_definition_load:
 * @func: (scope call):
 */
void
vector_definition_load (VectorDefinition *theme, VectorFunc func)
{
  func (theme);
}

void
vector_definition_set_background (VectorDefinition *theme, VectorColor background)
{
  theme->background = background;
}

void
vector_definition_add_layer (VectorDefinition *theme, gchar *name, VectorLayerFunc func)
{
  VectorLayer *layer = g_new0 (VectorLayer, 1);
  layer->name = name;
  layer->func = func;
  theme->layers = g_list_append (theme->layers, layer);
}

/**
 * vector_definition_get_layers:
 * Returns: (transfer none) (element-type VectorLayer): The layers in the theme
 */
GList*
vector_definition_get_layers (VectorDefinition *theme)
{
  return theme->layers;
}

/**
 * vector_definition_get_background:
 * Returns: (transfer none):
 */
VectorColor*
vector_definition_get_background (VectorDefinition *theme)
{
  return &theme->background;
}


static void
vector_definition_init (VectorDefinition *theme)
{
}

static void
vector_definition_class_init (VectorDefinitionClass *class)
{
}

VectorDefinition *
vector_definition_new (void)
{
  return g_object_new (VECTOR_TYPE_DEFINITION, NULL);
}


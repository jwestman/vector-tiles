#ifndef _vector_tile_feature_h_
#define _vector_tile_feature_h_

#include <glib-object.h>
#include "vector-parsing.h"
#include "vector-geometry.h"

// circular dependency from theme-tile-layer.h
typedef struct _VectorTileLayer VectorTileLayer;

#define VECTOR_TYPE_TILE_FEATURE (vector_tile_feature_get_type())
G_DECLARE_FINAL_TYPE(VectorTileFeature, vector_tile_feature, VECTOR, TILE_FEATURE, GObject)

VectorTileFeature* vector_tile_feature_new ();
void vector_tile_feature_load (VectorTileFeature *this,
                              VectorTileLayer *parent,
                              VectorDataChunk *data);

const gchar *vector_tile_feature_get_tag (VectorTileFeature *this,
                                          const gchar *key);
const gchar* vector_tile_feature_get_tag_localized (VectorTileFeature *this,
                                                    const gchar *key,
                                                    const gchar *locale);

VectorTileLayer *vector_tile_feature_get_parent (VectorTileFeature *this);
VectorGeometry *vector_tile_feature_get_geometry (VectorTileFeature *this);
guint64 vector_tile_feature_get_id (VectorTileFeature *this);


#endif /* _vector_tile_feature_h_ */

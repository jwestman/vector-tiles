#include <champlain/champlain.h>
#include <clutter/clutter.h>
#include "vector-renderer.h"
#include "vector-tile.h"

struct _VectorRenderer {
  ChamplainRenderer parent_instance;
  
  VectorDefinition *theme;
  const gchar *locale;

  guint8 *data;
  guint data_len;
};

G_DEFINE_TYPE(VectorRenderer, vector_renderer, CHAMPLAIN_TYPE_RENDERER)


void
vector_renderer_set_theme (VectorRenderer *this, VectorDefinition *theme)
{
  this->theme = theme;
  g_object_ref (theme);
}

void
vector_renderer_set_locale (VectorRenderer *this, const gchar *locale)
{
  this->locale = locale;
}

static void
set_data (ChamplainRenderer *renderer, const gchar *data, guint len)
{
  VectorRenderer *this = VECTOR_RENDERER (renderer);

  if (this->data) g_free (this->data);

  this->data = g_memdup (data, len);
  this->data_len = len;
}

static gboolean
do_render (ClutterCanvas *canvas, cairo_t *cairo, gint width, gint height, gpointer user_data)
{
  cairo_surface_t *surf = (cairo_surface_t*) user_data;
  cairo_set_source_surface (cairo, surf, 0, 0);
  cairo_paint (cairo);
  return FALSE;
}

static void
render (ChamplainRenderer *renderer, ChamplainTile *ctile)
{
  VectorRenderer *this = VECTOR_RENDERER (renderer);

  VectorDataChunk chunk;
  vector_data_chunk_init(&chunk, this->data, this->data_len);

  VectorTile *tile = vector_tile_new ();
  gint zoom_level = champlain_tile_get_zoom_level (ctile);
  vector_tile_load (tile, &chunk, this->theme, zoom_level);
  vector_tile_set_locale (tile, this->locale);

  guint size = champlain_tile_get_size (ctile);
  cairo_surface_t *surf = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, size, size);
  cairo_t *cairo = cairo_create (surf);

  vector_tile_draw (tile, cairo, size);

  champlain_exportable_set_surface (CHAMPLAIN_EXPORTABLE (ctile), surf);

  ClutterCanvas *canvas = CLUTTER_CANVAS (clutter_canvas_new ());
  clutter_canvas_set_size (canvas, size, size);
  g_signal_connect (canvas, "draw", (GCallback) do_render, surf);

  clutter_content_invalidate (CLUTTER_CONTENT (canvas));

  ClutterActor *actor = clutter_actor_new ();
  clutter_actor_set_size (actor, size, size);
  clutter_actor_set_content (actor, CLUTTER_CONTENT (canvas));

  champlain_tile_set_content (ctile, actor);
  g_signal_emit_by_name (ctile, "render-complete", this->data, this->data_len, FALSE);

  cairo_surface_destroy (surf);
  cairo_destroy (cairo);
  g_object_unref (tile);
  g_object_unref (canvas);
}


static void
vector_renderer_finalize (GObject *obj)
{
  VectorRenderer *this = VECTOR_RENDERER (obj);

  if (this->theme) g_object_unref (this->theme);
  if (this->data) g_free (this->data);

  G_OBJECT_CLASS (vector_renderer_parent_class)->finalize (obj);
}

static void
vector_renderer_init (VectorRenderer *this)
{
  this->data = NULL;
  this->theme = NULL;
}

static void
vector_renderer_class_init (VectorRendererClass *class)
{
  ChamplainRendererClass *renderer_class = CHAMPLAIN_RENDERER_CLASS (class);

  renderer_class->set_data = set_data;
  renderer_class->render = render;
  
  GObjectClass *obj_class = G_OBJECT_CLASS (class);
  obj_class->finalize = vector_renderer_finalize;
}

VectorRenderer *
vector_renderer_new (void)
{
  return g_object_new (VECTOR_TYPE_RENDERER, NULL);
}

